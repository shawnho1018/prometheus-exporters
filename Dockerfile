FROM golang:latest
ENV GO111MODULE on
ADD ./* /project/
WORKDIR /project/
RUN go mod init "prometheus-exporter"
RUN go build ./exporter.go
CMD ["./exporter", "-listen-address", "0.0.0.0:8080"]
