package main

import (
	"flag"
        "math"
	"log"
	"net/http"
	"time"
        "os"

        "github.com/golang/protobuf/proto"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
        dto "github.com/prometheus/client_model/go"
)

var addr = flag.String("listen-address", ":8080", "The address to listen on for HTTP requests.")
var (
	opsQueued = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "VMware",
		Subsystem: "custom_data",
		Name:      "ops_queued",
		Help:      "Number of gauge operations",
	})
        temps = prometheus.NewHistogram(prometheus.HistogramOpts{
                Namespace: "VMware",
                Subsystem: "custom_data",
                Name:    "pond_temperature_celsius",
                Help:    "The temperature of the frog pond.", 
                Buckets: prometheus.LinearBuckets(20, 5, 5),  
        })
)

func init() {
	prometheus.MustRegister(opsQueued)
        prometheus.MustRegister(temps)
}
func main() {
	flag.Parse()
        // add initial value into temps
        counter := 0
        for i := 0; i < 100; i++ {
           temps.Observe(30 + math.Floor(9*math.Sin(float64(i)*0.1*6.28)))
        }
        metric := &dto.Metric{}
        temps.Write(metric)
        log.SetOutput(os.Stdout)
        log.Println("Initial value")
        log.Println(proto.MarshalTextString(metric))
        log.SetFlags(log.Ldate|log.Ltime |log.LUTC)
	go func() {
		for {
			opsQueued.Add(4)
                        log.Printf("Add gauge by 1 and the result is %d\n", counter)
                        temps.Observe(30+math.Floor(9*math.Sin(float64(counter)*0.1*6.28)))
                        counter++
			time.Sleep(time.Second * 1)
		}
	}()
	http.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(*addr, nil))
}
